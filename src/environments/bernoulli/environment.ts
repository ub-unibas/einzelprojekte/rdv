import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  HistogramFieldModel,
  HistogramFieldType, initRdvLib, Page,
  SettingsModel,
  SortOrder,
  ViewerType,
  FacetValueOrder
} from 'rdv-lib'

export const environment: SettingsModel = {
  documentViewer: {
    // [ViewerType.MIRADOR]: { // not working if aot/prod is built
    'Mirador': {
      enabled: true,
      type: ViewerType.MIRADOR,
      enabledForPage: [{view: Page.Search, order: 10}],
      viewerUrl: "/assets/mirador/init.html",
      disabledPlugins: ["text-overlay"],
      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador.json",
      // Optional kann pro Breakpoint eine andere Konfiguration geladen werden.
      // Prinzipiell werden die Konfigurationen verwendet, wo die aktuelle Browserbreite unterhalb des maxWidth-Wertes liegt.
      // Von allen in Frage kommenden Definitionen wird diejenige mit der kleinsten Differenz von
      // "maxWidth - <aktuelle Breite>" verwendet.
      // Anm.: Die Reihenfolge der Definitionen ist egal.
      //viewerConfigMobilUrl: [
      // {maxWidth: 700, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"},
      //{maxWidth: 500, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"}
      //],
      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "digi",
          messageTitleKey: "search-results.view-mode.UV.object_type.message.title",
          messageTextKey: "Nur digitalisierte Objekte können in dieser Ansicht angezeigt werden.",
          values: [
            {label: "Ja", value: {"id": "Ja"}},
          ]
        }
      ],
    },
    'UV': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}, {view: Page.Detail, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/zas_en.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "digi",
          messageTitleKey: "search-results.view-mode.UV.object_type.message.title",
          messageTextKey: "Nur digitalisierte Objekte können in dieser Ansicht angezeigt werden.",
          values: [
            {label: "Ja", value: {"id": "Ja"}},
          ]
        }
      ],
    },
    'UVScroll': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/scroll_view.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "digi",
          messageTitleKey: "search-results.view-mode.UV.object_type.message.title",
          messageTextKey: "Nur digitalisierte Objekte können in dieser Ansicht angezeigt werden.",
          values: [
            {label: "Ja", value: {"id": "Ja"}},
          ]
        }
      ],
    },
    'MiradorSinglePage': {
      enabled: true,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_single.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "digi",
          messageTitleKey: "search-results.view-mode.UV.object_type.message.title",
          messageTextKey: "Nur digitalisierte Objekte können in dieser Ansicht angezeigt werden.",
          values: [
            {label: "Ja", value: {"id": "Ja"}},
          ]
        }
      ],
    },
  },

  production: false,

  showSimpleSearch: true,
  // viewerExtraWide: true,
  // per Default werden Preview Images angezeigt
  // showImagePreview: false,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "https://ub-bernoulli.ub.unibas.ch/",

  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: true
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  // footerSettings: {
  //   "default": {
  //     // den ganzen Footer ausblenden
  //     disable: false,
  //     // den "Nach Oben"-Link anzeigen
  //     displayToTop: true,
  //     // Host des Projekts im Footer anzeigen ("Universität Basel")
  //     displayHostUrl: true,
  //     // i18n key der URL des Host-Links (bei displayHostUrl: true)
  //     hostUrl: "footer.footerSettings.host.url",
  //     // i18n key des Namens des Host-Links (bei displayHostUrl: true)
  //     hostName: "footer.footerSettings.host.name",
  //     // Portal-URL im Footer anzeigen
  //     displayPortalUrl: true
  //   },
  //   // sprachspezifische Einstellungen (ohne Übersetzungen)
  //   "de": {
  //   },
  //   "en": {
  //   }
  // },

  // Dokument-Detail-Ansicht:
  // Mit 'bottom' oder 'top', wird in der Detail-Ansicht ein IIIF Viewer unterhalb oder oberhalb
  // der Felder eines Dokuments angezeigt. false schaltet die gleichzeitige Ansicht ab.
  narrowEmbeddedIiiFViewer: 'bottom',

  // UV/Mirador verwenden nur maximale Breite der app (false, undefined).
  // Wird diese Option auf true gesetzt, dann wird die volle Browser-Viewport-Breite verwendet.
  // viewerExtraWide: false,

  // die konkreten Werte, werden noch gesetzt
  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,

  externalAboutUrl: "https://ub.unibas.ch/de/portraets/",

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "year",
      order: SortOrder.ASC,
      display: "Datum (aufsteigend)"
    },
    {
      field: "year",
      order: SortOrder.DESC,
      display: "Datum (absteigend)"
    },
    {
      field: "Briefwechsel.label.keyword",
      order: SortOrder.ASC,
      display: "Briefwechsel (aufsteigend)"
    },
    {
      field: "Korrespondenz.label.keyword",
      order: SortOrder.ASC,
      display: "Korrespondenz (aufsteigend)"
    },
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "Briefwechsel": {
      "field": "Briefwechsel.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Briefwechsel",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "expandAmount": 10,
      "size": 100,
      "initiallyOpen": true,
      "valueOrder": FacetValueOrder.LABEL,
    },
    "Korrespondenz":
      {
        "field": "Korrespondenz.id.keyword",
        "facetType": FacetFieldType.SIMPLE,
        "label": "Korrespondenz",
        "operator": "OR",
        "operators": [
          "OR", "AND", "NOT"
        ],
        "order": 0,
        "expandAmount": 10,
        "size": 100,
        "searchWithin": true,
        "initiallyOpen": true,
        "valueOrder": FacetValueOrder.LABEL,
        "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT]
      },
    "Autor": {
      "field": "Autor.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "VerfasserIn",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "initiallyOpen": false,
      "autocomplete_size": 3,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT]
    },
    "Adressat": {
      "field": "Adressat.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "AdressatIn",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 2,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "initiallyOpen": false,
      "autocomplete_size": 3,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT]
    },
    "year": {
      "field": "year",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Datum",
      "operator": "AND",
      "showAggs": true,
      "order": 3,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "Zeitraum-Text": {
      "field": "046_c.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Datum - Text",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 4,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "initiallyOpen": false,
      "valueOrder": FacetValueOrder.LABEL
    },
    "751a": {
      "field": "751a.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Ort",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 10,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "initiallyOpen": false,
      "autocomplete_size": 3,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT]
    },
    "546_a": {
      "field": "546_a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Sprache",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 11,
      "expandAmount": 20,
      "size": 100,
      "searchWithin": false,
      "initiallyOpen": false,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "600_17_a": {
      "field": "600_17_a.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Thema - Person",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 12,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "initiallyOpen": false,
      "autocomplete_size": 3,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT]
    },
    "Bibliothek": {
      "field": "852_ab.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Standort",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 20,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "initiallyOpen": false,
      "valueOrder": FacetValueOrder.COUNT,
    },
    "hierarchy_filter": {
      "field": "hierarchy_filter.keyword",
      "facetType": FacetFieldType.HIERARCHIC,
      "label": "Bestandsgliederung",
      "operator": "AND",
      "order": 21,
      "size": 100,
      "expandAmount": 100,
      "initiallyOpen": false
    },
    "digi": {
      "field": "digi.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Digitalisat vorhanden",
      "operator": "OR",
      "order": 22,
      "size": 100,
      "expandAmount": 10,
      "initiallyOpen": false
    },

    "no_date": {
      "field": "no_date.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "no_date",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 33,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "initiallyOpen": false,
      "autocomplete_size": 3,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT]
    },
    "856_c": {
      "field": "856_c.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "856_c",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 43,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "initiallyOpen": false,
      "autocomplete_size": 3,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT]
    },
    "old_sys": {
      "field": "old_sysid.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "alte Systemnummer",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 44,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "initiallyOpen": false,
      "valueOrder": FacetValueOrder.LABEL
    },
    "bsiz_id": {
      "field": "bsiz_id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "ALMA IZ ID",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 45,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "initiallyOpen": false,
      "valueOrder": FacetValueOrder.LABEL
    }

  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {},

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [10, 50, 100, 200],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 50,
    "offset": 0,
    "sortField": "year",
    "sortOrder": SortOrder.ASC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
      "fct_location": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "fct_location",
      "label": "Geographikum"
      }
      },

  i18n: {
    "de": {
      "top.headerSettings.name": "Bernoulli Briefinventar (BIBB)",
      "top.headerSettings.name.Dev": "Bernoulli Briefinventar (BIBB) (Dev)",
      "top.headerSettings.name.Loc": "Bernoulli Briefinventar (BIBB) (Loc)",
      "top.headerSettings.name.Test": "Bernoulli Briefinventar (BIBB) (Test)",
      "top.headerSettings.betaBarContact.name": "Bernoulli Euler Zentrum",
      "top.headerSettings.betaBarContact.email": "bez@unibas.ch",
      "search-results.title": "{{value}} Briefe",
    },
    "en": {
      "top.headerSettings.name": "Bernoulli Briefinventar (BIBB)",
      "top.headerSettings.name.Dev": "Bernoulli Briefinventar (BIBB) (Dev)",
      "top.headerSettings.name.Loc": "Bernoulli Briefinventar (BIBB) (Loc)",
      "top.headerSettings.name.Test": "Bernoulli Briefinventar (BIBB) (Test)",
      "top.headerSettings.betaBarContact.name": "Bernoulli Euler Zentrum",
      "top.headerSettings.betaBarContact.email": "bez@unibas.ch",
      "search-results.title": "{{value}} Briefe",
    }
  },

};
initRdvLib(environment);
