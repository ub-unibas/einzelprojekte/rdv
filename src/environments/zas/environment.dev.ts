import {initRdvLib, SettingsModel} from 'rdv-lib';
import {environment as proj} from '@env/zas/environment';
import {environment as dev} from '@env_temp/environment.type-dev';
import {addDevNamePostfix} from "@env_temp/util";

const Proj = "zas-dev";

export const environment: SettingsModel = {
  ...proj,
  ...dev,
  headerSettings: addDevNamePostfix(proj.headerSettings),

  proxyUrl : dev.proxyUrl +  Proj + "/",
  moreProxyUrl: dev.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: dev.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: dev.detailProxyUrl +  Proj + "/",
  documentViewerProxyUrl: dev.documentViewerProxyUrl +  Proj + "/",
  navDetailProxyUrl: dev.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: dev.popupQueryProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: dev.suggestSearchWordProxyUrl + Proj + "/",
  detailNewProxyUrl: dev.detailNewProxyUrl + Proj + "/",
};

initRdvLib(environment);
