/*
 * Public API Surface of rdv-lib
 */

// NOTE: export * ... seems not to work if symlinks are used in rdv-app!

export {initRdvLib} from './lib/init';

// ---- core -----------------------------------------------------------

export {CoreModule} from './lib/core/core.module';
export {GetRemoteFilterFieldOptions, OptionRetrieveError, RemoteFilterConfigsActions, RemoteFilterConfigsActionTypes,
SetRemoteFilterFieldOptions} from './lib/core/actions/remote-filter-configs.actions';
export {AboutComponent} from './lib/core/components/about.component';
export {BetaHeaderComponent} from './lib/core/components/beta-header.component';
// export * from './lib/core/components/detailed-view.component';
export {FooterComponent} from './lib/core/components/footer.component';
export {HeaderComponent} from './lib/core/components/header.component';
export {RemoteFilterConfigsEffects} from './lib/core/effects/remote-filter-configs.effects';
export {
  State as RemoteFilterConfigState,
  initialState as remoteFilterConfigInitialState,
  reducer as remoteFilterConfigReducer,
  initRemoteFilterConfigsReducer
} from './lib/core/reducers/remote-filter-configs.reducer';
export {AbstractAppComponent} from './lib/core/components/app.component';

// ---- reducers -------------------------------------------------------

export {
  State as UserConfigState,
  getFilterFields,
  getFilterFieldsByKey,
  getUserConfigState,
  metaReducers,
  reducers as coreReducers,
  initCoreReducers
} from './lib/reducers/index';

// ---- search ---------------------------------------------------------

export {SearchModule} from './lib/search/search.module';
export {AddBasket, AddBaskets, BasketActions, BasketActionTypes, ClearBaskets, DeleteBasket,
SelectBasket, UpdateBasket, UpsertBasket} from './lib/search/actions/basket.actions';
export {AddBasketResults, BasketResultActions, BasketResultActionTypes, ClearBasketResults
} from './lib/search/actions/basket-result.actions';
export {AddDetailedResult, ClearDetailedResults, DeleteDetailedResult, DetailedResultActions,
DetailedResultActionTypes} from './lib/search/actions/detailed-result.actions';
export {
  ResetAll as ResetAllFacetAction, FacetActions, FacetActionTypes, UpdateFacetFields, UpdateFacetHistograms,
  UpdateFacetQueries, UpdateFacetRanges, UpdateHierarchicFacets, UpdateTotal
} from './lib/search/actions/facet.actions';
export {
  UpdateFacetValueOrder,
  KeepSearchFilters,
  ClearAllInFacetSearch,
  SetFacetOpenedInUi,
  ResetHierarchicFacetValueTo,
  ResetHistogramBoundaryTo,
  UpdateFacetOperator,
  AddHierarchicFacetValue,
  AddHistogramBoundaries,
  AddFacetValue,
  FormActionTypes,
  ResetAll as ResetAllFormAction,
  FormActions,
  RangeBoundariesChanged,
  RemoveAllFacetValue,
  RemoveAllHierarchicFacetValue,
  RemoveAllHistogramBoundary,
  RemoveFacetValue,
  RemoveHierarchicFacetValue,
  RemoveHistogramBoundary,
  ResetRange,
  SelectFacets,
  SetInFacetSearch,
  SetShowMissingValuesInRange,
  ShowMissingValuesInRange,
  ToggleFilterValue,
  UpdateEntireForm,
  UpdateRangeBoundaries,
  UpdateSearchFieldType,
  UpdateSearchFieldValue
} from './lib/search/actions/form.actions';
export {LayoutActions, LayoutActionTypes, ShowFacetOrRange, ToggleMapDisplay, ToggleTimelineDisplay
} from './lib/search/actions/layout.actions';
export {SuggestSearchWordSuccess, MakeSuggestEditFieldSearchSuccess, MakeSuggestEditFieldSearch,
MakeSuggestEditFieldSearchFailure, SuggestSearchWordSearch, SuggestSearchWordFailure,
MakeSaveDetailedEditRequest, ResetQuery, InFacetSearch, SimpleSearch, SetSortField, SetResultListDisplayMode,
QueryActionTypes, BasketSearchFailure, BasketSearchSuccess, DetailedSaveFailure, DetailedSaveSuccess,
DetailedSearchFailure, DetailedSearchSuccess, InFacetSearchSuccess, MakeBasketSearchRequest, MakeDetailedEditRequest,
MakeDetailedSearchRequest, MakeInFacetSearchRequest, MakeNextDetailedSearchRequest, MakePreviousDetailedSearchRequest,
MakeSearchRequest, NextPage, NextPageSuccess, QueryActions, Reset, SearchFailure, SearchSuccess, SetOffset,
SetRows, SetSortOrder} from './lib/search/actions/query.actions';
export {FacetSelection, UpdatePopup, UpdateIIIFUrl, AddResults, ClearResults, CommonSelectedFacetValue, ResultActions,
ResultActionTypes} from './lib/search/actions/result.actions';
export {AddSavedQueries, AddSavedQuery, DeleteSavedQuery, SavedQueryActions, SavedQueryActionTypes
} from './lib/search/actions/saved-query.actions';
export {SetSearchWordSuggestionsResults, SuggestSearchWordResultActionTypes, SuggestSearchWordResultActions
} from './lib/search/actions/suggest-search-word.actions';

export {AbstractHistogramFacetComponentDirective, EmittedValues, MinMaxProvider, ValidatorResult
} from './lib/search/components/abstract-histogram-facet-component.directive';
export {AbstractMinMaxInputComponentDirective} from './lib/search/components/abstract-min-max-input-component.directive';
export {AutocompleteComponent} from './lib/search/components/autocomplete.component';
export {BasketIconComponent} from './lib/search/components/basket-icon.component';
export {BasketListComponent} from './lib/search/components/basket-list.component';
export {BasketResultsListComponent} from './lib/search/components/basket-results-list.component';
export {CollapsibleComponent} from './lib/search/components/collapsible.component';
export {CollapsibleFacetsComponent} from './lib/search/components/collapsible-facets.component';
export {CopyLinkComponent} from './lib/search/components/copy-link.component';
export {DateHierarchicFacetComponent} from './lib/search/components/date-hierarchic-facet.component';
export {DateHistogramFacetComponent} from './lib/search/components/date-histogram-facet.component';
export {DetailedComponent} from './lib/search/components/detailed.component';
export {DetailedFieldComponent} from './lib/search/components/detailed-field.component';
export {DocViewModeComponent} from './lib/search/components/doc-view-mode.component';
export {EditFieldComponent} from './lib/search/components/edit-field.component';
export {ExpandableComponent} from './lib/search/components/expandable.component';
export {ExportResultsListComponent} from './lib/search/components/export-results-list.component';
export {FacetOrderSelectorComponent} from './lib/search/components/facet-order-selector.component';
export {FacetsComponent} from './lib/search/components/facets.component';
export {FieldsComponent} from './lib/search/components/fields.component';
export {FiltersComponent} from './lib/search/components/filters.component';
export {GeoHierarchicFacetComponent} from './lib/search/components/geo-hierarchic-facet.component';
export {GeoHistogramFacetComponent} from './lib/search/components/geo-histogram-facet.component';
export {HierachicFacetComponent} from './lib/search/components/hierachic-facet.component';
export {InFacetSearchComponent} from './lib/search/components/in-facet-search.component';
export {IntHierarchicFacetComponent} from './lib/search/components/int-hierarchic-facet.component';
export {IntHistogramFacetComponent} from './lib/search/components/int-histogram-facet.component';
export {ManageSavedQueriesComponent} from './lib/search/components/manage-saved-queries.component';
export {ManageSearchComponent} from './lib/search/components/manage-search.component';
export {MinMaxDateInputComponent} from './lib/search/components/min-max-date-input.component';
export {MinMaxGeoInputComponent} from './lib/search/components/min-max-geo-input.component';
export {MinMaxIntInputComponent} from './lib/search/components/min-max-int-input.component';
export {MiradorComponent} from './lib/search/components/mirador.component';
export {NewSearchComponent} from './lib/search/components/new-search.component';
export {OperatorSelectorComponent} from './lib/search/components/operator-selector.component';
export {ParamsSetComponent} from './lib/search/components/params-set.component';
export {LOCALIZED_ROUTER_PREFIX, PopupLandingComponent} from './lib/search/components/popup-landing.component';
export {RangesComponent} from './lib/search/components/ranges.component';
export {ResultHeaderComponent} from './lib/search/components/result-header.component';
export {ResultListsComponent} from './lib/search/components/result-lists.component';
export {ResultPagingComponent} from './lib/search/components/result-paging.component';
export {ResultRowComponent} from './lib/search/components/result-row.component';
export {RowsPerPageComponent} from './lib/search/components/rows-per-page.component';
export {SaveQueryComponent} from './lib/search/components/save-query.component';
export {SearchComponent} from './lib/search/components/search.component';
export {SearchHitComponent} from './lib/search/components/search-hit.component';
export {SearchHitValueComponent} from './lib/search/components/search-hit-value.component';
export {SearchParamsComponent} from './lib/search/components/search-params.component';
export {SearchResultsComponent} from './lib/search/components/search-results.component';
export {SearchResultsListComponent} from './lib/search/components/search-results-list.component';
export {SelectedFacetsComponent} from './lib/search/components/selected-facets.component';
export {SimpleCollapsibleFacetComponent} from './lib/search/components/simple-collapsible-facet.component';
export {SimpleSearchAutocompleteComponent} from './lib/search/components/simple-search-autocomplete.component';
export {SimpleSearchComponent} from './lib/search/components/simple-search.component';
export {SimpleFacetComponent} from './lib/search/components/simple-facet.component';
export {SubcatFacetComponent} from './lib/search/components/subcat-facet.component';
export {TinymceComponent} from './lib/search/components/tinymce.component';
export {TopComponent} from './lib/search/components/top.components';
export {UniversalViewerComponent} from './lib/search/components/universal-viewer.component';
export {UvManifestLinkComponent} from './lib/search/components/uv-manifest-link.component';
export {ValidatableListComponent} from './lib/search/components/validatable-list.component';
export {VisualSearchComponent} from './lib/search/components/visual-search.component';

export {FormEffects} from './lib/search/effects/form.effects';
export {QueryEffects} from './lib/search/effects/query.effects';

export {Basket} from './lib/search/models/basket.model';
export {BasketResult} from './lib/search/models/basket-result.model';
export {DetailedResult} from './lib/search/models/detailed-result.model';
export {Doc, Result, DocTranslation} from './lib/search/models/result.model';
export {SavedQuery} from './lib/search/models/saved-query.model';

export {
  State as BasketState,
  reducer as basketReducer,
  initialState as basketInitialState,
  adapter as basketAdapter,
  selectAll as basketSelectAll,
  selectEntities as basketSelectEntities,
  selectIds as basketSelectIds,
  selectTotal as basketSelectTotal,
  selectCurrentBasketId
}from './lib/search/reducers/basket.reducer';
export {
  reducer as basketResultReducer, initialState as basketResultInitialState, State as BasketResultState,
  adapter as basketResultAdapter, selectAll as basketResultSelectaAll, selectEntities as basketResultSelectEntities,
  selectIds as basketResultSelectIds, selectTotal as basketResultSelectTotal
} from './lib/search/reducers/basket-result.reducer';
export {
  selectTotal as detailedResultSelectTotal, selectIds as detaildResultSelectIds,
  selectEntities as detailedResultSelectEntities, selectAll as detailedResultSelectAll,
  adapter as detailedResultAdapter, State as DetailedResultState, initialState as detailedResultInitialState,
  reducer as detailedResultReducer
} from './lib/search/reducers/detailed-result.reducer';
export {
  reducer as facetReducer,
  initialState as facetInitialState,
  State as FacetState,
  buildId as FacetBuildId,
  initFacetReducer
} from './lib/search/reducers/facet.reducer';
export {
  State as FormState,
  initialState as formInitialState,
  reducer as FormReducer,
  SelectedFacetValue as FormSelectedFacetValue,
  SelectedFacet as FormSelectedFacet,
  SearchField as FormSearchField,
  SelectedFacetMap as FormSelectedFacetMap,
  SelectedHistogramFacetMap as FormSelectedHistogramFacetMap,
  SelectedHistogramFacetValue as FormSelectedHistogramFacetValue,
  initFormReducer
} from './lib/search/reducers/form.reducer';
export {
  State as SearchStateInterface,
  reducers as searchReducers,
  getInitialCombinedNewSearchQueries,
  keepSearchFilters,
  _getAllSuggestions,
  getAllInFacetSearchValues,
  getFacetValues,
  getSearchValues,
  getHierarchyValues,
  getPopup,
  getHistogramValues,
  getAllBasketResults,
  getAllBaskets,
  getAllDetailedResults,
  getAllOpenFacet,
  getAllResults,
  getAllSavedQueries,
  getAllSuggestions,
  getBasketCount,
  getBasketEntities,
  getBasketIds,
  getCombinedNewSearchQueries,
  getCombinedSearchQueries,
  getCurrentBasket,
  getCurrentBasketElementsCount,
  getCurrentBasketId,
  getCurrentBasketsDisplayedRows,
  getDetailedResultsIds,
  getDisplayMap,
  getDisplayTimeline,
  getFacetFieldCount,
  getFacetFieldCountByKey,
  getFacetHistogramCount,
  getFacetHistogramCountByKey,
  getFacetQueryCount,
  getFacetQueryCountByKey,
  getFacetRangeCount,
  getFacetValuesByKey,
  getFilterValuesByKey,
  getFormValues,
  getHierarchicFacetCount,
  getHierarchicFacetCountByKey,
  getIIIFResultUrl,
  getQueryParams,
  getRangeValues,
  getRangeValuesByKey,
  getResultOffset,
  getResultRows,
  getResultSortField,
  getResultSortOrder,
  getSavedQueriesCount,
  getSavedQueryEntities,
  getSearchValuesByKey,
  getShownFacetOrRange,
  getTotalResultsCount,
  getUsedResultViewer,
  SearchState
} from './lib/search/reducers/index';
export {
  State as LayoutState, reducer as layoutReducer, initialState as layoutInitialState, initLayoutReducer
} from './lib/search/reducers/layout.reducer';
export {
  initialState as queryInitialState, reducer as queryReducer, State as QueryState, initQueryReducer
} from './lib/search/reducers/query.reducer';
export {
  selectTotal as resultSelectTotal,
  selectIds as resultSelectIds,
  selectEntities as resultSelectedEntities,
  selectAll as resultSelectAll,
  adapter as resultAdapter,
  initialState as resultInitialState,
  reducer as resultReducer,
  State as ResultState,
  PopupType
} from './lib/search/reducers/result.reducer';
export {
  State as SavedQueryState,
  reducer as savedQueryReducer,
  initialState as savedQueryInitialState,
  adapter as savedQueryAdapter,
  selectAll as savedQuerySelectAll,
  selectEntities as savedQuerySelectEntities,
  selectIds as savedQuerySelectIds,
  selectTotal as savedQuerySelectTotal
} from './lib/search/reducers/saved-query.reducer';
export {
  selectTotal as suggestSearchWordResultSelectTotal,
  selectIds as suggestSearchWordResultSelectIds,
  selectEntities as suggestSearchWordResultSelectEntities,
  selectAll as suggestSearchWordResultSelectAll,
  adapter as suggestSearchWordResultAdapter,
  initialState as suggestSearchWordResultInitialState,
  reducer as suggestSearchWordResultReducer,
  State  as SuggestSearchWordResultState
} from './lib/search/reducers/suggest-search-word-result.reducer';

export {BrokenImageLinkDirective} from './lib/search/shared/directives/broken-image-link.directive';
export {DisplayLinkDirective} from './lib/search/shared/directives/display-link.directive';
export {PopperDirective} from './lib/search/shared/directives/popper.directive';
export {SafePipe} from './lib/search/shared/directives/safe-src.directive';

// ---- shared ---------------------------------------------------------

export {environment} from './lib/shared/Environment';
export {deepJsonClone, empty, hashCodeFromString, isObject, memoize, mergeDeep, randomHashCode, stringToEnumValue
} from './lib/shared/utils';
export {
  FieldCase as ModelFieldCase,
  ValueCase as ModelValueCase,
  FieldType as ModelFieldType,
  LocalizedLabel as ModelLocalizedLabel,
  BasicAutoCompleteValue as ModelBasicAutoCompleteValue,
  DocField as ModelDocField,
  Doc as ModelDoc,
  AutoCompleteValue as ModelAutoCompleteValue,
  DocFieldValue as ModelDocFieldValue,
  EditDoc as ModelEditDoc,
  LocalizedValue as ModelLocalizedValue,
  DocFieldEdit as ModelDocFieldEdit,
  EditDocField as ModelEditDocField,
  FacetSelection as ModelFacetSelection,
  FacetValue as ModelFacetValue,
  FuncMetaData as ModelFuncMetaData,
  GroupInfo as ModelGroupInfo,
  SelectedAutoCompletValue as ModelSelectedAutoCompletValue,
  SelectedFacetValues as ModelSelectedFacetValues
} from './lib/shared/models/doc-details.model';
export {searchParamObserver} from './lib/shared/models/observed-util';
export {QueryFormat, CommonQueryFormatFacet, QueryFormatFacetField, QueryFormatFacetFieldMap, QueryFormatFilterFieldMap,
QueryFormatHierarchicFacetField, QueryFormatHierarchicFacetFieldMap, QueryFormatHistogramFacetField,
QueryFormatHistogramFacetFieldMap, QueryFormatRangeFieldMap, QueryFormatSearchFieldMap, QueryParams
} from './lib/shared/models/query-format';
export {FacetResponseMap, FacetResponse, SearchResponse} from './lib/shared/models/response-format';
export {HeaderSettingsModel, HeaderSettings, headerSettingsModelByLanguage, FacetFieldType, FacetValueOrder,
ListType, ViewerType, BackendViewerType, Page, QueryParamsModel, DocumentViewer, ResultView, BaseSettings,
SettingsModel, FooterSettingsModel, FooterSettings, Backend, CommonFacetFieldModel, ExtraInfoFieldType,
ExtraInfosModel, FacetFieldModel, FacetFieldsModel, footerSettingsModelByLanguage, HistogramFieldModel,
HistogramFieldType, IIIFRestriction, IiifViewerMobileConfig, RangeFieldModel, RangeFieldsModel, SortFieldsModel,
SortOrder, TableColumnModel, ViewOrder} from './lib/shared/models/settings.model';
export {defaultViewer, possibleViewers, availableIiifViewers, orderOfViewer, checkViewersFromBackend
} from './lib/shared/models/settings-util';

export {PipesModule} from './lib/shared/pipes/index';
export {ObjectKeysPipe} from './lib/shared/pipes/object-keys.pipe';
export {SafeHtmlPipe} from './lib/shared/pipes/safeHtml.pipe';

export {backendSearchServiceProvider} from './lib/shared/services/backend-search.service.provider';
export {BackendSearchService} from './lib/shared/services/backend-search.service';
export {BasketRequestInterceptor} from './lib/shared/services/basket-request-interceptor.service';
export {DateFormatService} from './lib/shared/services/date-format.service';
export {DetailedRequestInterceptor} from './lib/shared/services/detailed-request-interceptor.service';
export {ElasticBackendSearchService} from './lib/shared/services/elastic-backend-search.service';
export {EnvTranslateCompiler} from './lib/shared/services/env.translate.compiler';
export {LocalizedText, I18nToastrService} from './lib/shared/services/i18n-toastr.service';
export {PreviousRouteServiceProvider} from './lib/shared/services/previous-route-service-provider.service';
export {ResetUtilService} from './lib/shared/services/reset-util.service';
export {SearchRequestInterceptor} from './lib/shared/services/search-request-interceptor.service';
export {SolrBackendSearchService} from './lib/shared/services/solr-backend-search.service';
export {SelectedFacet, UrlParamsService, SearchActions, FacetValueProcessor, ReplaceUrlParams,
SearchValueProcessor, SelectedHierarchicFacet, SelectedHistogram, UrlParamNames, ViewerValueProcessor
} from './lib/shared/services/url-params.service';
export {URLSearchParamsUrlSerializer} from './lib/shared/services/URLSearchParamsUrlSerializer';
