/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {Result} from '../models/result.model';
import {ResultActions, ResultActionTypes} from '../actions/result.actions';

export interface PopupType {
  /**
   * Localized key of the message to display.
   */
  i18nKey: string;
  /**
   * Optional arguments for the localized key.
   */
  values?: { [key: string]: (boolean | string | number) };
}

/**
 * List of currently shown results
 */
export interface State extends EntityState<Result> {
  /**
   * If search result is displayed in IIIF Universal Viewer.
   */
  iiifUrl?: string;
  /**
   * Show popup.
   */
  popup?: PopupType
}

/**
 * @ignore
 */
export const adapter: EntityAdapter<Result> = createEntityAdapter<Result>();

/**
 * @ignore
 */
export const initialState: State = adapter.getInitialState({});

/**
 * @ignore
 */
export function reducer(
  state = initialState,
  action: ResultActions
): State {
  switch (action.type) {
    case ResultActionTypes.AddResults: {
      const newState = adapter.addMany(action.payload.results, state);
      newState.iiifUrl = state.iiifUrl;
      return newState;
    }
    case ResultActionTypes.ClearResults: {
      const newState = adapter.removeAll(state);
      newState.iiifUrl = state.iiifUrl;
      return newState;
    }
    case ResultActionTypes.UpdateIIIFUrl: {
      return {...state, iiifUrl: action.url}
    }
    case ResultActionTypes.UpdatePopup: {
      return {...state, popup: action.popup}
    }

    default: {
      return state;
    }
  }
}

/**
 * @ignore
 */
export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
