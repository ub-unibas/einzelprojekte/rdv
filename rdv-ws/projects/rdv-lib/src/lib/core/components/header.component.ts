/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component} from '@angular/core';

/**
 * Main navigation container
 */
@Component({
  selector: 'app-header',
  template: `
    <!-- <div class="container mb-2">
      <nav class="nav nav-pills mt-3">
        <a class="px-2 py-1 btn btn-outline-primary mr-2"
           routerLink=""
           [routerLinkActiveOptions]="{ exact: true }"
           routerLinkActive="active">{{'header.database' | translate}}</a>
        <a class="btn btn-outline-primary px-2 py-1 mr-2"
           [routerLink]="['/about' | localize]"
           routerLinkActive="active">{{'header.about' | translate}}</a>
        <a class="btn btn-outline-primary px-2 py-1"
           [routerLink]="['/old-search' | localize]"
           routerLinkActive="active">{{'header.old_search' | translate}}</a>
      </nav>
    </div> -->
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
}
