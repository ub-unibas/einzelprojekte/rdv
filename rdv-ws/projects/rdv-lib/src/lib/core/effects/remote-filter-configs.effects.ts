/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {map} from 'rxjs/operators';

import * as fromActions from '../actions/remote-filter-configs.actions';

/**
 * Effects for configuration changes at runtime
 */
@Injectable()
export class RemoteFilterConfigsEffects {

  /**
   * Requests document data from backend on action {@link GetRemoteFilterFieldOptions}
   */
  @Effect()
  remoteFilterFieldOptions$ = this._actions$.pipe(
    ofType(fromActions.RemoteFilterConfigsActionTypes.GetRemoteFilterFieldOptions),
    map((action: fromActions.GetRemoteFilterFieldOptions) =>
      this._http.get(action.payload.url)
        .subscribe(
          (res: string) => new fromActions.SetRemoteFilterFieldOptions({key: action.payload.key, value: res}),
          (err) => new fromActions.OptionRetrieveError(err))));

  /**
   * @ignore
   */
  constructor(private _actions$: Actions, private _http: HttpClient) {
  }
}
