The file "init.html" and the directory static/ are created by this sub-project "mirador-with-plugins".

This package is automatically build if "npm run build-prod" is called in rdv-ws/.

For "npm run build-dev" call "npm run build-mirador" once before in rdv-ws/.

TODO

- is static/css/ needed? init.htlm doesn't really include it!?
- ff the plugin "OSDReferences" is added, the following error is displayed and mirador doesn't load:
  Error: You must pass a component to the function returned by connect. Instead received undefined
  All other plugins work and this plugin does almost nothing.
- a lot of unmet deps: but it seems to work without them!?
    "@material-ui/core": "4.11.0",
    "@material-ui/icons": "4.9.1",
    "@material-ui/lab": "4.0.0-alpha.53",
    "@blueprintjs/core": "3.31.0",
    "@blueprintjs/icons": "3.20.1"
    "isomorphic-unfetch": "^3.0.0",
    "lodash": "^4.17.11",
    "manifesto.js": "^4.1.0",
    "prop-types": "^15.6.2",
    "redux-saga": "^1.1.3",
    "reselect": "^4.0.0",
    "@babel/runtime": "7.11.2",
    "react-copy-to-clipboard": "5.0.1",

