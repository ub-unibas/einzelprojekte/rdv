#!/bin/sh
rm -rf ../../dist/mirador-with-plugins
cd projects/mirador-with-plugins
yarn install
npm run build
rm -f build/asset-manifest.json
mv build/index.html build/init.html
mkdir -p ../../dist/
rm -rf ../../dist/mirador-with-plugins
mv build ../../dist/mirador-with-plugins
