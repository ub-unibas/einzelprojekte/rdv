# Research Data Viewer (RDV)

## Configuration

The application is highly configurable by providing a configuration file. A couple of examples
can be found in `src/environments`:

* `elastic` (default) und `production` (production-ready, i.e. optimized for performance)
* `freidok` and `freidok-prod` (production-ready)
* `bwsts` and `bwsts-prod` (production-ready)
* `elastic-mh` and `elastic-mh-prod` (production-ready)

In order to use another configuration than the default one, start the server with the `-c` option and 
provide the name of the desired configuration, e.g. `ng serve -c freidok-prod`.

### Additional configurations

1. Copy an existing configuration and change the deviating values. The new configuration should implement
`SettingsModel`.
2. In the `angular.json` file, add respective configuration blocks in the `targets` where you want to use
your new configuration (i.e. at least the `build` and `serve` targets with paths 
`targets.build.configurations.new_config` and `targets.serve.configurations.new_config`).

An example configuration:

```json
{
  "targets": {
    "build": {
      "configurations": {
        "new_config": {
          "optimization": true,
          "outputHashing": "all",
          "sourceMap": false,
          "extractCss": true,
          "namedChunks": false,
          "aot": true,
          "extractLicenses": true,
          "vendorChunk": false,
          "buildOptimizer": true,
          "fileReplacements": [
            {
              "replace": "src/environments/environment.ts",
              "with": "src/environments/environment.new_config.ts"
            }
          ]
        }
      }
    } 
  }
}
```

Apart from `fileReplacements` all fields can be left out. To avoid repetition, you can also link
a settings block for a configuration from another target definition:

```json
{
  "targets": {
    "serve": {
      "configurations": {
        "new_config": {
          "browserTarget": "rdv:build:new_config"
        }
      }
    }
  }
}
```

__Beware__: If you adjust the index and/or the type of the Elasticsearch cluster, you have to change also the respective settings in `angularx_elasticsearch_proxy_unibas.php`.


## Building documentation

You can build documentation locally with [compodoc](https://compodoc.github.io/compodoc/):

1. Install: `yarn global add @compodoc/compodoc` or `npm -g install @compodoc/compodoc`
2. Run in application root folder: `compodoc -swp src/tsconfig.app.json`


## Fonts

"Universität Basel" licensed some fonts which can't be published under
MIT license. To use your own fonts, edit src/scss/styles.scss,
comment out '@import "fonts"' and uncomment '@import "fonts-custom"'.

Finally place your font faces and declarations in src/scss/fonts-custom.scss.

## Build dev application

Initially: for e.g. zas-dev env

1. cd rdv-ws
1. yarn install # once
1. yarn run build-mirador
1. yarn run build-dev
1. cd ..
1. yarn install
1. ng serve -c zas-dev --host 0.0.0.0

Note: If you use the provided docker-compose env, simply
prefix each command with "./in-node.sh" which targets
the outer project and "./in-node-ws.sh" which targets 
something in rdv-ws/. Stay in the project's main dir
to call ./in-node-ws.sh" (skip cd rdv-ws).

On changes in rdv-ws/projects/rdv-lib:

1. cd rdv-ws
1. yarn run build-dev
1. cd ..
1. yarn upgrade rdv-lib
   `# this triggers a watching "ng serve" call, but compilation will fail
   so restart "ng serve ..."`
1. ng serve -c zas-dev --host 0.0.0.0 (development) or ng serve -c zas-loc --host 0.0.0.0 --port 4201 (local on a specific port)


## For Afrika-Portal

`ng serve -c afrikaportal-loc --host 0.0.0.0`

see it on http://localhost:4200

it will use the query builder on http://127.0.0.1:5000/v1/rdv_query/es_proxy/, cf. environment.type-loc.ts

## Requirements

1. you need angular cli : sudo npm i -g @angular/cli
2. you need a recent version of node (at least 14)
   1. sudo npm install -g n
   2. sudo n stable
3. might need to clear npm cache mpm cache clean -f

## Build prod application

Example for zas-dev env:

1. cd rdv-ws
2. yarn install # once
3. yarn run build-mirador
4. yarn run build-prod
5. cd ..
6. yarn upgrade rdv-lib
7. ng build --configuration=production,zas-dev
8. cp .htaccess dist/

The whole app is in dist/ ready for distribution.

## Dockerize and deploy on k8s 
1. build the prod application (see above)
2. docker build --network host --no-cache --rm=true -t rdv_africa_viewer_[environment] .
3. docker tag rdv_africa_viewer_[environment] registry.gitlab.com/ub-unibas/package-registry/rdv_africa_viewer_[environment]
4. docker login https://registry.gitlab.com/ub-unibas/package-registry
5. docker push registry.gitlab.com/ub-unibas/package-registry/rdv_africa_viewer_[environment]:latest
6. kubectl apply -f k8s-manifests/afrikaportal/[environment]/

## Run locally in docker

docker run -p 80:80 rdv_africa_viewer_[environment]

and go to localhost in your browser

